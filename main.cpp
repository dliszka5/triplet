#include <iostream>

using namespace std;

template<typename firstType, typename secondType, typename thirdType> class Triplet{
	firstType  	first;
	secondType 	second;
	thirdType 	third;
public:
	Triplet(firstType first = firstType(), 
		  secondType second = secondType(), 
		  thirdType third = thirdType()) : 
	first(first), second(second), third(third)  { }

	Triplet(const Triplet& obiekt){
		this->first = obiekt.first;
		this->second = obiekt.second;
		this->third = obiekt.third;
	}

		const void setFirst(const firstType& first){
			this->first = first;
		}

		const void setSecond(const secondType& second){
			this->second = second;
		}

		const void setThird(const thirdType& third){
			this->third = third;
		}

		const firstType getFirst() const {
			return this->first;
		}

		const secondType getSecond() const {
			return this->second;
		}

		const thirdType getThird() const {
			return this->third;
		}


		Triplet& make_triplet(const firstType& first, const secondType& second, const thirdType& third){
			this->first = first;
			this->second = second;
			this->third = third;
				return *this;
		}

		const bool isEmpty() const {
			if(this->first){
				if(this->second){
					if(this->third)
						return false;
				}
			}

			return true;
		}

		const bool operator==(const Triplet& obiekt) const {
			if(this->first == obiekt.first){
				if(this->second == obiekt.second)
					if(this->third == obiekt.third)
						return true;
			}
			return false;
		}

		const bool operator!=(const Triplet& obiekt) const {
			if(this->first == obiekt.first){
				if(this->second == obiekt.second)
					if(this->third == obiekt.third)
						return false;
			}
			return true;
		}

		Triplet& operator=(Triplet& obiekt){
			this->first = obiekt.first;
			this->second = obiekt.second;
			this->third = obiekt.third;

			return *this;
		}

		const Triplet& operator=(const Triplet& obiekt){
			this->first = obiekt.first;
			this->second = obiekt.second;
			this->third = obiekt.third;
			return *this;
		}


		friend const ostream& operator<<(ostream& out, Triplet& obiekt) {
			out << "first = " << obiekt.first << " second = " << obiekt.second <<" third = " << obiekt.third;
			return out;
		}

		friend const istream& operator>>(istream& in, Triplet& obiekt){
			in >> obiekt.first >> obiekt.second >> obiekt.third;
				return in;
		}

		virtual ~Triplet() { }

		/*
			swap
			isEmpty
			operator==
			operator !=


		*/
	
};

template<typename type> class TComplex{
	type im;
	type re;
public:
	TComplex(type re = 0, type im = 0) : re(re),im(im) { }
	TComplex(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
	}
	void setRe(const type re){
		this->re = re;
	}

	void setIm(const type im){
		this->im = im;
	}

	const type getRe() const {
		return re;
	}

	const type getIm() const {
		return im;
	}

	const TComplex& operator=(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
			return *this;
	}

	const void operator+(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+=(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-=(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+(){
		this->re = +this->re;
		this->im = +this->im;
	}

	const void operator-(){
		this->re = -this->re;
		this->im = -this->im;
	}

    const TComplex& operator*(const TComplex& comp){
        TComplex *temp = new TComplex;
        temp->re=(re*comp.re)+(im*comp.im);
        temp->im=(re*comp.im)+(im*comp.re);
        return *temp;
    }



	friend ostream& operator<<(ostream& out,const TComplex<type>& obiekt){
		out << "Re = " << obiekt.re << " Im = " << obiekt.im;
		return out;
	}

	friend istream& operator>>(istream& in,TComplex<type>& obiekt){
		in >> obiekt.re >> obiekt.im;
		return in;
	}

	virtual ~TComplex() { }	
};

int main(){

	Triplet<int,char,string> obiekt1(1,2,"gitara");

	cout << obiekt1.getThird() << endl;

	Triplet< int, char, TComplex<double> > obiekt2;

	cout << obiekt2.getThird() << endl;

	return 0;
}